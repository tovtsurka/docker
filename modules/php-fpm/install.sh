#!/bin/sh
COLOR='\033[0;35m'

echo -e "${COLOR}Run application installer: install.sh"
cd /var/www/html;

chmod 774 /var/www/html/init
composer global require --optimize-autoloader \
        "hirak/prestissimo:0.3.7" && \
     composer global dumpautoload --optimize && \
     composer clear-cache;

composer update;
composer install;

php /var/www/html/init --env=docker;
php /var/www/html/yii migrate up --interactive=0;
chown -R www-data:www-data /var/www/html;